<?php
require_once './libreria.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php css(); ?>
        <title><?= $titulo ?></title>
    </head>
    <body>
        <?php $m=menu() ?>
        <?= contenido($datos) ?>
        <?= js(); ?>
    </body>
</html>

