<?php

function css() {
    ?>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <?php
}

function js() {
    $r = '<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>';
    echo $r;
}

function resultados($ejercicio) {
    switch ($ejercicio) {
        case 'ejercicio1':
            require './inc/resultados1.inc';
            break;
        case 'ejercicio2':
            require './inc/resultados2.inc';
            break;
        case 'ejercicio3s':
            $numeros = $_GET["numeros"];
            $suma = 0;
            $resto = explode(";", $numeros[2]);
            unset($numeros[2]);
            $numeros = array_merge($numeros, $resto);
            foreach ($numeros as $numero) {
                $suma += $numero;
            }
            require './inc/resultados3s.inc';
            break;
        case 'ejercicio3p':
            $numeros = $_GET["numeros"];
            $producto = 1;
            $resto = explode(";", $numeros[2]);
            unset($numeros[2]);
            $numeros = array_merge($numeros, $resto);
            foreach ($numeros as $numero) {
                $producto *= $numero;
            }
            require './inc/resultados3p.inc';
            break;
    }
}

/**
 *
 * @param string $ejercicio formulario que quiero que muestre
 * @param bool $validar  si quiero que realice la validacion de campos
 */
function formularios($ejercicio, $validar = true) {
    switch ($ejercicio) {
        case 'ejercicio1':
            require './inc/formulario1.inc';
            break;
        case 'ejercicio2':
            require './inc/formulario2.inc';
            break;
        case 'ejercicio3':
            require './inc/formulario3.inc';
            break;
        case 'ejercicio3s':
            require './inc/formulario3s.inc';
            break;
        case 'ejercicio3p':
            require './inc/formulario3p.inc';
            break;
    }
}

function contenido($datos) {
    extract($datos); // crea una variable por cada indice
    require "./inc/$pagina.inc"; // carga dinamica
}

function cargar($datos) {
    extract($datos); // crea una variable por cada indice del array
    require "./layout/layout1.inc";
}

function galeria($numero) {
    $galerias = [
        [
            [
                "titulo" => "Foto 1",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f1.jpg",
                "fecha" => "1/1/2022"
            ],
            [
                "titulo" => "Foto 2",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f2.jpg",
                "fecha" => "1/1/2022"
            ],
            [
                "titulo" => "Foto 3",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f3.jpg",
                "fecha" => "1/1/2022"
            ],
            [
                "titulo" => "Foto 4",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f4.jpg",
                "fecha" => "1/1/2022"
            ]
        ],
        [
            [
                "titulo" => "Foto 5",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f5.jpg",
                "fecha" => "1/1/2022"
            ],
            [
                "titulo" => "Foto 6",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f6.jpg",
                "fecha" => "1/1/2022"
            ],
            [
                "titulo" => "Foto 7",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f7.jpg",
                "fecha" => "1/1/2022"
            ],
            [
                "titulo" => "Foto 8",
                "texto" => "lorem ipsum",
                "src" => "./imgs/f8.jpg",
                "fecha" => "1/1/2022"
            ]
        ]
    ];

    require './inc/galerias.inc';
}

/*
 * La funcion menu en vez de mostrar el menu lo devuelve
 */

function menu() {
    ob_start();
    require "./inc./menu.inc";
    return ob_get_clean();
}
?>

