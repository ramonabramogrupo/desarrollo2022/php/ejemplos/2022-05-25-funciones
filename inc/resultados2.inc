<div class="col-lg-8 row mx-auto">
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Email</div>
            <div class="card-body">
                <p class="card-text"><?= $_GET["correo"] ?></p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Password</div>
            <div class="card-body">
                <p class="card-text"><?= $_GET["password"] ?></p>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-8 row mx-auto mt-2">
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Mes de Acceso</div>
            <div class="card-body">
                <p class="card-text"><?= $_GET["mes"] ?></p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Formas de Acceso</div>
            <div class="card-body">
                <?php
                if (!isset($_GET["formas"])) {
                    echo "<p class=\"card-text\">No has seleccionado ninguna forma de acceso</p>";
                } else {
                    foreach ($_GET["formas"] as $valor) {
                        echo "<p class=\"card-text\">$valor</p>";
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-8 row mx-auto mt-2">
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Ciudad</div>
            <div class="card-body">
                <p class="card-text"><?= $_GET["ciudad"] ?></p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Navegadores Utilizados</div>
            <div class="card-body">
                <?php
                if (!isset($_GET["navegador"])) {
                    echo "<p class=\"card-text\">no hay navegadores seleccionados</p>";
                } else {
                    foreach ($_GET["navegador"] as $valor) {
                        echo "<p class=\"card-text\">$valor</p>";
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

