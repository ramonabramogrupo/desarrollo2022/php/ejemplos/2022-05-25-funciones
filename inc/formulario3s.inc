<form>
    <div class="form-group row">
        <label for="numero1" class="col-lg-2 col-form-label">Numero 1</label>
        <div class="col-lg-10">
            <input type="number" class="form-control" id="numero1" placeholder="Numero entero" name="numeros[]" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="numero2" class="col-lg-2 col-form-label">Numero 2</label>
        <div class="col-lg-10">
            <input type="number" class="form-control" id="numero2" placeholder="Numero entero" name="numeros[]" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="numeros" class="col-lg-2 col-form-label">Numeros</label>
        <div class="col-lg-10">
            <input type="text" class="form-control" id="numeros" placeholder="Numeros separados por ;" name="numeros[]">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-6">
            <button name="ejercicio3s" class="btn btn-primary">Sumar</button>
        </div>
    </div>
</form>

