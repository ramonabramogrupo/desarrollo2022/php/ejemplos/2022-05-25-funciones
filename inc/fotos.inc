<div class="col-lg-3">
    <div class="card mb-3">
        <img class="card-img-top" src="<?= $foto["src"] ?>" data-toggle="modal" data-target="<?= "#foto$numero" ?>">
        <div class="card-body">
            <h5 class="card-title"><?= $foto["titulo"] ?></h5>
            <p class="card-text"><?= $foto["texto"] ?></p>
            <p class="card-text"><small class="text-muted"><?= $foto["fecha"] ?></small></p>
        </div>
    </div>
</div> 

<div id="<?= "foto$numero" ?>" class="modal fade">
    <div class="modal-dialog">
        <div class="model-content">
            <img src="<?= $foto["src"]?>" class="modal-lg" data-dismiss="modal">
        </div>
    </div>
</div>


