<div class="col-lg-8 row mx-auto">
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Producto</div>
            <div class="card-body">
                <p class="card-text"><?= $producto ?></p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Numeros introducidos</div>
            <div class="card-body">
                <?php
                foreach ($numeros as $valor) {
                    echo "<p class=\"card-text\">$valor</p>";
                }
                ?>
            </div>
        </div>
    </div>
</div>